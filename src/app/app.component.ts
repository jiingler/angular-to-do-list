import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('myInput') myInput: ElementRef;
  title = 'angular-to-do-list';

  list = [];

  ngOnInit() {
    this.list = ['Hit the gym', 'Pay bills', 'Meet George', 'Buy eggs', 'Read a book', 'Organize office'];
  }

  newElement(value) {
    this.myInput.nativeElement.value = '';
    this.list.push(value);
  }

  checkItem(item) {
    if (item.target.className === 'checked') {
      item.target.className = '';
    } else {
      item.target.className = 'checked';
    }
  }

  deleteItem($event, idx) {
    $event.stopPropagation();
    this.list.splice(idx, 1);
  }
}
